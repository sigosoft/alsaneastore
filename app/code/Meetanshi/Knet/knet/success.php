<?php

use Magento\Framework\App\Bootstrap;

include('../app/bootstrap.php');
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
$request = $objectManager->get('Magento\Framework\App\Request\Http');
$helper = $objectManager->get('Meetanshi\Knet\Helper\Data');
$redirect = $objectManager->create('Magento\Framework\App\Response\Http');
$params = $request->getParams();
$baseUrl = $storeManager->getStore()->getBaseUrl();

$ResErrorText = isset($params['ErrorText']) ? $params['ErrorText'] : '';
$ResPaymentId = isset($params['paymentid']) ? $params['paymentid'] : '';
$ResTrackID = isset($params['trackid']) ? $params['trackid'] : '';
$ResErrorNo = isset($params['Error']) ? $params['Error'] : '';
$ResResult = isset($params['result']) ? $params['result'] : '';
$ResPosdate = isset($params['postdate']) ? $params['postdate'] : '';
$ResTranId = isset($params['tranid']) ? $params['tranid'] : '';
$ResAuth = isset($params['auth']) ? $params['auth'] : '';
$ResAVR = isset($params['avr']) ? $params['avr'] : '';
$ResRef = isset($params['ref']) ? $params['ref'] : '';
$ResAmount = isset($params['amt']) ? $params['amt'] : '';
$TranData = isset($params['trandata']) ? $params['trandata'] : '';

$terminalResKey = $helper->getResourceKey();

if ($ResErrorText == null && $ResErrorNo == null) {
    $ResTranData = $TranData;
    if ($ResTranData != null) {
        $url = $baseUrl . "knet/payment/success";
        $decrytedData = $helper->decrypt($ResTranData, $terminalResKey);
        return $redirect->setRedirect($url . '?' . $decrytedData)->sendResponse();
    }
} else {
    $url = $baseUrl . "knet/payment/cancel";
    $result_params = "?ErrorText=" . $ResErrorText . "&trackid=" . $ResTrackID . "&amt=" . $ResAmount . "&paymentid=" . $ResPaymentId;
    return $redirect->setRedirect($url . $result_params)->sendResponse();
}

