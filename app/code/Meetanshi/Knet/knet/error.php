<?php

use Magento\Framework\App\Bootstrap;

include('../app/bootstrap.php');
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
$request = $objectManager->get('Magento\Framework\App\Request\Http');
$redirect = $objectManager->create('Magento\Framework\App\Response\Http');
$params = $request->getParams();
$baseUrl = $storeManager->getStore()->getBaseUrl();

$ResErrorText = isset($params['ErrorText']) ? $params['ErrorText'] : '';
$ResPaymentId = isset($params['paymentid']) ? $params['paymentid'] : '';
$ResTrackID = isset($params['trackid']) ? $params['trackid'] : '';
$ResErrorNo = isset($params['Error']) ? $params['Error'] : '';
$ResResult = isset($params['result']) ? $params['result'] : '';
$ResPosdate = isset($params['postdate']) ? $params['postdate'] : '';
$ResTranId = isset($params['tranid']) ? $params['tranid'] : '';
$ResAuth = isset($params['auth']) ? $params['auth'] : '';
$ResAVR = isset($params['avr']) ? $params['avr'] : '';
$ResRef = isset($params['ref']) ? $params['ref'] : '';
$ResAmount = isset($params['amt']) ? $params['amt'] : '';
$TranData = isset($params['trandata']) ? $params['trandata'] : '';

$url = $baseUrl . "knet/payment/cancel";
$result_params = "?paymentid=" . $ResPaymentId . "&result=" . $ResResult . "&postdate=" . $ResPosdate . "&tranid=" . $ResTranId . "&auth=" . $ResAuth . "&ref=" . $ResRef . "&trackid=" . $ResTrackID;
return $redirect->setRedirect($url . $result_params)->sendResponse();
