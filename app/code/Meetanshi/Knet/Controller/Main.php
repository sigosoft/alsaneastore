<?php

namespace Meetanshi\Knet\Controller;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\Order\Payment\Transaction\Builder;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\Knet\Helper\Data as HelperData;

abstract class Main extends Action
{
    protected $customerSession;
    protected $checkoutSession;
    protected $registry;
    protected $orderFactory;
    protected $helper;
    protected $storeManager;
    protected $order;
    protected $scopeConfig;
    protected $request;
    protected $transactionBuilder;
    protected $resultPageFactory;
    protected $e24PaymentPipe;

    public function __construct(Context $context, Registry $registry, Session $checkoutSession, StoreManagerInterface $storeManager, ScopeConfigInterface $scopeConfig, OrderFactory $orderFactory, Http $request, Builder $transactionBuilder, HelperData $helper, PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->registry = $registry;
        $this->orderFactory = $orderFactory;
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->request = $request;
        $this->transactionBuilder = $transactionBuilder;
        $this->resultPageFactory = $resultPageFactory;
    }

    public function getOrder()
    {
        if ($this->order == null) {
            $session = $this->checkoutSession;
            $this->order = $this->orderFactory->create();
            $this->order->loadByIncrementId($session->getLastRealOrderId());
        }
        return $this->order;
    }

    protected function getConfig($path)
    {
        $path = 'payment/knet/' . $path;
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
