<?php

namespace Meetanshi\Knet\Controller\Payment;

use Meetanshi\Knet\Controller\Main;
use Magento\Framework\Controller\ResultFactory;

class Success extends Main
{
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $params = $this->getRequest()->getParams();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderIncrementId = $objectManager->create('\Magento\Sales\Model\ResourceModel\Order\Collection')->getLastItem()->getIncrementId();

        $order = $this->orderFactory->create()->loadByIncrementId($orderIncrementId);
        $amount = round($order->getGrandTotal(), 3);
        $payment = $order->getPayment();

        $paymentID = isset($params['paymentid']) ? $params['paymentid'] : '';
        $presult = isset($params['result']) ? $params['result'] : '';
        $postdate = isset($params['postdate']) ? $params['postdate'] : '';
        $tranid = isset($params['tranid']) ? $params['tranid'] : '';
        $auth = isset($params['auth']) ? $params['auth'] : '';
        $ref = isset($params['ref']) ? $params['ref'] : '';
        $trackid = isset($params['trackid']) ? $params['trackid'] : '';


        if ($presult == 'CAPTURED') {
            $message = 'KNET Payment Details:<br/>';
            if ($paymentID) {
                $message .= 'PaymentID: ' . $paymentID . "<br/>";
            }
            if ($amount) {
                $message .= 'Amount: ' . $amount . "<br/>";
            }
            if ($presult) {
                $message .= 'Result: ' . $presult . "<br/>";
            }
            if ($postdate) {
                $message .= 'PostDate: ' . $postdate . "<br/>";
            }
            if ($tranid) {
                $message .= 'TranID: ' . $tranid . "<br/>";
            }
            if ($auth) {
                $message .= 'Auth: ' . $auth . "<br/>";
            }
            if ($ref) {
                $message .= 'Ref: ' . $ref . "<br/>";
            }
            if ($trackid) {
                $message .= 'TrackID: ' . $trackid . "<br/>";
            }
            $payment->setTransactionId($tranid);
            $payment->setLastTransId($tranid);
            $payment->setAdditionalInformation('paymentid', $paymentID);
            $payment->setAdditionalInformation('result', $presult);
            $payment->setAdditionalInformation('tranid', $tranid);
            $payment->setAdditionalInformation('auth', $auth);
            $payment->setAdditionalInformation('track_id', $trackid);

            $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());
            $trans = $this->transactionBuilder;
            $transaction = $trans->setPayment($payment)->setOrder($order)->setTransactionId($tranid)->setAdditionalInformation((array)$payment->getAdditionalInformation())->setFailSafe(true)->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->addTransactionCommentsToOrder($transaction, $message);
            $payment->setParentTransactionId(null);

            $payment->save();

            $objectManager->create('\Magento\Sales\Model\OrderNotifier')->notify($order);

            $order->addCommentToStatusHistory(__('Transaction is approved by the bank'), \Magento\Sales\Model\Order::STATE_PROCESSING)->setIsCustomerNotified(true);
            $this->messageManager->addSuccessMessage(__('Transaction is approved by the bank'));
            $order->save();

            $transaction->save();

            $result_params = "?paymentid=" . $paymentID . "&amount=" . $amount . "&result=" . $presult . "&tranid=" . $tranid . "&auth=" . $auth . "&ref=" . $ref . "&trackid=" . $trackid . "&postdate=" . $postdate;

            $this->_redirect('checkout/onepage/success'.$result_params);
        }
        if ($presult == 'NOT CAPTURED' || $presult == 'NOT+CAPTURED') {
            $message = 'KNET Payment Details:<br/>';
            if ($paymentID) {
                $message .= 'PaymentID: ' . $paymentID . "<br/>";
            }
            if ($amount) {
                $message .= 'Amount: ' . $amount . "<br/>";
            }
            if ($presult) {
                $message .= 'Result: ' . $presult . "<br/>";
            }
            if ($postdate) {
                $message .= 'PostDate: ' . $postdate . "<br/>";
            }
            if ($tranid) {
                $message .= 'TranID: ' . $tranid . "<br/>";
            }
            if ($auth) {
                $message .= 'Auth: ' . $auth . "<br/>";
            }
            if ($ref) {
                $message .= 'Ref: ' . $ref . "<br/>";
            }
            if ($trackid) {
                $message .= 'TrackID: ' . $trackid . "<br/>";
            }

            $payment->setAdditionalInformation('paymentid', $paymentID);
            $payment->setAdditionalInformation('result', $presult);
            $payment->setAdditionalInformation('tranid', $tranid);
            $payment->setAdditionalInformation('auth', $auth);
            $payment->setAdditionalInformation('track_id', $trackid);

            $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());

            $order->addCommentToStatusHistory($message, \Magento\Sales\Model\Order::STATE_CANCELED);

            $order->cancel()->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true, 'Transaction is not approved by the bank');
            $payment->setStatus('CANCEL');
            $payment->setShouldCloseParentTransaction(1)->setIsTransactionClosed(1);
            $payment->save();
            $this->checkoutSession->restoreQuote();
            $this->messageManager->addErrorMessage(__('Transaction is not approved by the bank'));
            $order->save();
            $result_params = "?paymentid=" . $paymentID . "&amount=" . $amount . "&result=" . $presult . "&tranid=" . $tranid . "&auth=" . $auth . "&ref=" . $ref . "&trackid=" . $trackid . "&postdate=" . $postdate;
            $this->_redirect('knet/payment/fail'.$result_params);
        }
        if ($presult == 'CANCELED') {
            $message = 'KNET Payment Details:<br/>';
            if ($paymentID) {
                $message .= 'PaymentID: ' . $paymentID . "<br/>";
            }
            if ($amount) {
                $message .= 'Amount: ' . $amount . "<br/>";
            }
            if ($presult) {
                $message .= 'Result: ' . $presult . "<br/>";
            }
            if ($postdate) {
                $message .= 'PostDate: ' . $postdate . "<br/>";
            }
            if ($tranid) {
                $message .= 'TranID: ' . $tranid . "<br/>";
            }
            if ($auth) {
                $message .= 'Auth: ' . $auth . "<br/>";
            }
            if ($ref) {
                $message .= 'Ref: ' . $ref . "<br/>";
            }
            if ($trackid) {
                $message .= 'TrackID: ' . $trackid . "<br/>";
            }

            $payment->setAdditionalInformation('paymentid', $paymentID);
            $payment->setAdditionalInformation('result', $presult);
            $payment->setAdditionalInformation('tranid', $tranid);
            $payment->setAdditionalInformation('auth', $auth);
            $payment->setAdditionalInformation('track_id', $trackid);

            $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());

            $order->addCommentToStatusHistory($message, \Magento\Sales\Model\Order::STATE_CANCELED);

            $order->cancel()->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true, 'Transaction is not approved by the bank');
            $payment->setStatus('CANCEL');
            $payment->setShouldCloseParentTransaction(1)->setIsTransactionClosed(1);
            $payment->save();
            $this->checkoutSession->restoreQuote();
            $this->messageManager->addErrorMessage(__('Transaction is not approved by the bank.'));
            $order->save();
            $result_params = "?paymentid=" . $paymentID . "&amount=" . $amount . "&result=" . $presult . "&tranid=" . $tranid . "&auth=" . $auth . "&ref=" . $ref . "&trackid=" . $trackid . "&postdate=" . $postdate;
            $this->_redirect('knet/payment/fail'.$result_params);
        }
    }
}
