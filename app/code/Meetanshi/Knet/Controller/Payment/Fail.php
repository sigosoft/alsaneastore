<?php

namespace Meetanshi\Knet\Controller\Payment;

use Meetanshi\Knet\Controller\Main;

class Fail extends Main
{
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
