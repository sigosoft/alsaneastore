<?php

namespace Meetanshi\Knet\Controller\Payment;

use Meetanshi\Knet\Controller\Main;

class Redirect extends Main
{
    public function execute()
    {
        $orderIncrementId = $this->checkoutSession->getLastRealOrderId();

        $order = $this->orderFactory->create()->loadByIncrementId($orderIncrementId);

        $successUrl = $this->storeManager->getStore()->getUrl('', ['_secure' => true]) . 'knet/success.php';
        $errorUrl = $this->storeManager->getStore()->getUrl('', ['_secure' => true]) . 'knet/error.php';

        $tranTrackid = $orderIncrementId;
        $translID = $this->helper->getTransportalId();
        $transportalID = "id=" . $this->helper->getTransportalId();
        $transportalPass = "password=" . $this->helper->getTransportalPassword();
        $amount = "amt=" . round($order->getBaseGrandTotal(), 3);
        $trackID = "trackid=" . $tranTrackid;
        $currency = "currencycode=414";
        $language = "langid=" . $this->helper->getPaymentLanguage();
        $action = "action=1";
        $responseURL = "responseURL=" . $successUrl;
        $errorURL = "errorURL=" . $errorUrl;

        $paymentUrl = $this->helper->getGatewayUrl();

        $termResourceKey = $this->helper->getResourceKey();

        $param = $transportalID . "&" . $transportalPass . "&" . $action . "&" . $language . "&" . $currency . "&" . $amount . "&" . $responseURL . "&" . $errorURL . "&" . $trackID;
        $params = $this->helper->encryptAES($param, $termResourceKey) . "&tranportalId=" . $translID . "&responseURL=" . $successUrl . "&errorURL=" . $errorURL;

        $url = $paymentUrl . "&trandata=" . $params;

        $resultRedirect = $this->resultRedirectFactory->create();

        $message = 'Customer is redirected to Knet';

        $order->setState(\Magento\Sales\Model\Order::STATE_NEW, true, $message);
        $order->save();

        return $resultRedirect->setUrl($url);

    }
}
