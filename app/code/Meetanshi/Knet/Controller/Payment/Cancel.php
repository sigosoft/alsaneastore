<?php

namespace Meetanshi\Knet\Controller\Payment;

use Meetanshi\Knet\Controller\Main;

class Cancel extends Main
{
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $params = $this->getRequest()->getParams();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderIncrementId = $objectManager->create('\Magento\Sales\Model\ResourceModel\Order\Collection')->getLastItem()->getIncrementId();
        $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($orderIncrementId);

        $errorMsg = __('Transaction was not Successful. Your Order was not Compleated Please try again later');

        $amount = round($order->getGrandTotal(), 3);

        $paymentID = isset($params['paymentid']) ? $params['paymentid'] : '';
        $presult = isset($params['result']) ? $params['result'] : '';
        $postdate = isset($params['postdate']) ? $params['postdate'] : '';
        $tranid = isset($params['tranid']) ? $params['tranid'] : '';
        $auth = isset($params['auth']) ? $params['auth'] : '';
        $ref = isset($params['ref']) ? $params['ref'] : '';
        $trackid = isset($params['trackid']) ? $params['trackid'] : '';

        $message = 'KNET Payment Details:<br/>';
        if ($paymentID) {
            $message .= 'PaymentID: ' . $paymentID . "<br/>";
        }
        if ($amount) {
            $message .= 'Amount: ' . $amount . "<br/>";
        }
        if ($presult) {
            $message .= 'Result: ' . $presult . "<br/>";
        }
        if ($postdate) {
            $message .= 'PostDate: ' . $postdate . "<br/>";
        }
        if ($tranid) {
            $message .= 'TranID: ' . $tranid . "<br/>";
        }
        if ($auth) {
            $message .= 'Auth: ' . $auth . "<br/>";
        }
        if ($ref) {
            $message .= 'Ref: ' . $ref . "<br/>";
        }
        if ($trackid) {
            $message .= 'TrackID: ' . $trackid . "<br/>";
        }
        $payment = $order->getPayment();
        $order->cancel()->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true, 'Gateway has declined the payment.');
        $payment->setStatus('DECLINED');
        $payment->setShouldCloseParentTransaction(1)->setIsTransactionClosed(1);
        $payment->save();
        $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
        $order->addStatusToHistory($order->getStatus(), $message);
        $this->messageManager->addErrorMessage($errorMsg);
        $this->checkoutSession->restoreQuote();
        $order->save();
        return $resultRedirect->setPath('checkout/cart');
    }
}
