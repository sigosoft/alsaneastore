<?php

namespace Meetanshi\Knet\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Meetanshi\Knet\Helper\Deploy;

class InstallData implements InstallDataInterface
{
    protected $deployHelper;

    public function __construct(Deploy $deployHelper
    )
    {
        $this->deployHelper = $deployHelper;
    }

    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $this->deployAxis();
    }

    protected function deployAxis()
    {
        $p = strrpos(__DIR__, DIRECTORY_SEPARATOR);
        $modulePath = $p ? substr(__DIR__, 0, $p) : __DIR__;
        $modulePath .= '/knet';
        $this->deployHelper->deployAxis($modulePath);
    }
}
