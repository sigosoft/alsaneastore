<?php

namespace Meetanshi\Knet\Block;

use Magento\Payment\Block\Form as PaymentForm;

class Form extends PaymentForm
{
    protected $_template = 'Meetanshi_Knet::form.phtml';

    protected $_methodCode = 'knet';

    public function getMethodCode()
    {
        return $this->_methodCode;
    }
}
