<?php

namespace Meetanshi\Knet\Block;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class Payment extends Template
{
    protected $_checkoutSession;

    protected $_orderCollectionFactory;

    public function __construct(Context $context, CheckoutSession $checkoutSession, CollectionFactory $orderCollectionFactory, array $data = [])
    {
        parent::__construct($context, $data);
        $this->_checkoutSession = $checkoutSession;
    }

    public function getOrderId()
    {
        $orderId = $this->_checkoutSession->getLastRealOrderId();
        if (!$orderId) {
            $orderId = $this->_checkoutSession->getKnetOrder();
        }
        return $orderId;
    }

    public function getSorder()
    {
        $this->orders = $this->_orderCollectionFactory->create()->getLastItem()->getIncrementId();
        return $this->orders;
    }
}
