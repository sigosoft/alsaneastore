<?php

namespace Meetanshi\Knet\Block\Payment;

use Magento\Payment\Block\ConfigurableInfo;

class Info extends ConfigurableInfo
{
    protected $_template = 'Meetanshi_Knet::payment/info.phtml';

    public function getLabel($field)
    {
        switch ($field) {
            case 'method_title':
                return __('Method Title');
            case 'paymentid':
                return __('Payment Id');
            case 'result':
                return __('Transaction Status');
            case 'tranid':
                return __('Transaction ID');
            case 'auth':
                return __('Authorization Number');
            case 'track_id':
                return __('Tracking Id');
            default:
                return parent::getLabel($field);
        }
    }
}
