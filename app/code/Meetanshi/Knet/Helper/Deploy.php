<?php

namespace Meetanshi\Knet\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem;

class Deploy extends AbstractHelper
{
    const FILE_PERMISSIONS = 0666;
    const DIR_PERMISSIONS = 0777;

    protected $write;
    protected $read;
    protected $filesystem;

    public function __construct(Context $context, Filesystem $filesystem)
    {
        parent::__construct($context);

        $this->filesystem = $filesystem;
        $this->write = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
        $this->read = $filesystem->getDirectoryRead(DirectoryList::ROOT);
    }

    public function deployAxis($folder)
    {
        $from = $this->read->getRelativePath($folder);
        $this->moveFiles($from, '');
    }

    public function moveFiles($from, $to)
    {
        $baseName = basename($from);
        $files = $this->read->readRecursively($from);
        array_unshift($files, $from);

        foreach ($files as $file) {
            $fileName = $this->getFilePath(
                $file,
                $from,
                ltrim($to . '/' . $baseName, '/')
            );

            if ($this->read->isExist($fileName)) {
                continue;
            }

            if ($this->read->isFile($file)) {
                $this->write->copyFile($file, $fileName);

                $this->write->changePermissions(
                    $fileName,
                    self::FILE_PERMISSIONS
                );
            } elseif ($this->read->isDirectory($file)) {
                $this->write->create($fileName);

                $this->write->changePermissions(
                    $fileName,
                    self::DIR_PERMISSIONS
                );
            }
        }
    }

    protected function getFilePath($path, $from, $to)
    {
        return str_replace($from, $to, $path);
    }
}
