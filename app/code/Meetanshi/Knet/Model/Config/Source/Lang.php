<?php

namespace Meetanshi\Knet\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Lang implements Arrayinterface
{
    public function toOptionArray()
    {
        return [['value' => 'USA', 'label' => __('English')], ['value' => 'AR', 'label' => __('Arabic')],];
    }
}
