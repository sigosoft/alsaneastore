<?php

namespace Meetanshi\Knet\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Checkout\Model\Session;

class KnetConfigProvider implements ConfigProviderInterface
{
    protected $localeResolver;

    protected $config;

    protected $currentCustomer;

    protected $methods = [];

    protected $paymentHelper;

    protected $checkoutSession;

    public function __construct(ResolverInterface $localeResolver, CurrentCustomer $currentCustomer, Session $checkoutSession, PaymentHelper $paymentHelper)
    {
        $this->localeResolver = $localeResolver;
        $this->currentCustomer = $currentCustomer;
        $this->paymentHelper = $paymentHelper;
        $this->checkoutSession = $checkoutSession;

        $code = 'knet';
        $this->methods[$code] = $this->paymentHelper->getMethodInstance($code);
    }

    public function getConfig()
    {
        $code = 'knet';
        $config = [];

        if ($this->methods[$code]->isAvailable($this->checkoutSession->getQuote())) {
            $config = [];
            $config['payment'] = [];
            $config['payment']['knet']['redirectUrl'] = [];
            $config['payment']['knet']['redirectUrl'][$code] = $this->getMethodRedirectUrl($code);
            $config['payment']['knet'][$code]['instructions'] = $this->methods[$code]->getInstructions();
        }

        return $config;
    }

    protected function getMethodRedirectUrl($code)
    {
        return $this->methods[$code]->getOrderPlaceRedirectUrl();
    }
}
