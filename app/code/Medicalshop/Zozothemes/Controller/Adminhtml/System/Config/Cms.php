<?php

namespace Medicalshop\Zozothemes\Controller\Adminhtml\System\Config;

abstract class Cms extends \Magento\Backend\App\Action {
    protected function _import()
    {
        return $this->_objectManager->get('Medicalshop\Zozothemes\Model\Import\Cms')
            ->importCms($this->getRequest()->getParam('import_type'));
    }
}
