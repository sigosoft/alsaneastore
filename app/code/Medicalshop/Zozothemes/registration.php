<?php
/**
 * Copyright © 2016 by Zozothemes. All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Medicalshop_Zozothemes',
    __DIR__
);
