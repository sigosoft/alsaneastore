var config = {
    map: {
        '*': {
            fancybox_button: 'Medicalshop_Zozothemes/js/fancybox/jquery.fancybox-buttons',
            fancybox_media: 'Medicalshop_Zozothemes/js/fancybox/jquery.fancybox-media',
            fancybox_thumbs: 'Medicalshop_Zozothemes/js/fancybox/jquery.fancybox-thumbs',
            fancybox: 'Medicalshop_Zozothemes/js/fancybox/jquery.fancybox',
            fancybox_pack: 'Medicalshop_Zozothemes/js/fancybox/jquery.fancybox.pack',
            mousewheel: 'Medicalshop_Zozothemes/js/fancybox/jquery.mousewheel-3.0.6.pack',
            stickyhead: 'Medicalshop_Zozothemes/js/sticky/jquery.sticky',
            easingjs_other: 'Medicalshop_Zozothemes/js/others/jquery.easing.1.3',
            scroll_overflow: 'Medicalshop_Zozothemes/js/fullpagescroller/scrolloverflow.min',
            full_pagelib: 'Medicalshop_Zozothemes/js/fullpagescroller/jquery.fullpage'
            
        }
    },
    paths: {
        "jquery.bootstrap": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min"
    },
    shim: {
        'fancybox':{
            'deps':['jquery']
        },
        'fancybox_pack':{
            'deps':['jquery']
        },
        'jquery.bootstrap': {
            'deps': ['jquery']
        },
        'easingjs_other': {
            'deps': ['jquery']
        },
        'scroll_overflow': {
            'deps': ['jquery']
        },
        'full_pagelib': {
            'deps': ['jquery']
        }
    }
};

