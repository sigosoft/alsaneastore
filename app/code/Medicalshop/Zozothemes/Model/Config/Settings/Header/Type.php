<?php
namespace Medicalshop\Zozothemes\Model\Config\Settings\Header;

class Type implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => '1', 'label' => __('Type 1')], ['value' => '2', 'label' => __('Type 2')], ['value' => '5', 'label' => __('Type 3')]];
    }

    public function toArray()
    {
        return ['1' => __('Type 1'), '2' => __('Type 2'), '5' => __('Type 3')];
    }
}
