<?php
namespace Medicalshop\Countdown\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $dbVersion = $context->getVersion();
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($dbVersion, '1.0.1', '<')) { // update an existing product attribute to add as soring options
            $eavSetup->updateAttribute(
                'catalog_product', 'countdown_enabled', array('attribute_code' => 'medicalshop_countdown_enabled'), 'label', 'Medicalshop Countdown'
            );
        }
		if (version_compare($dbVersion, '1.0.2', '<')) { // update an existing product attribute to add as soring options
            $eavSetup->updateAttribute(
                 'catalog_product', 'medicalshop_countdown_enabled', 'frontend_label', 'Medicalshop Countdown'
            );
        }
    }
}
