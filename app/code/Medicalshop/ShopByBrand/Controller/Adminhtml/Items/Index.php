<?php
/** 
  * Zozothemes.
  * 
  * NOTICE OF LICENSE
  * 
  * This source file is subject to the Zozothemes.com license that is
  * available through the world-wide-web at this URL:
  * http://www.zozothemes.com/license-agreement.html
  * 
  * DISCLAIMER
  * 
  * Do not edit or add to this file if you wish to upgrade this extension to newer
  * version in the future.
  * 
  * @category   Medicalshop
  * @package    Medicalshop_ShopByBrand
  * @copyright  Copyright (c) 2014 Zozothemes (http://www.zozothemes.com/)
  * @license    http://www.zozothemes.com/LICENSE-1.0.html
  */

namespace Medicalshop\ShopByBrand\Controller\Adminhtml\Items;

class Index extends \Medicalshop\ShopByBrand\Controller\Adminhtml\Items
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('medicalshop::base');
        $resultPage->getConfig()->getTitle()->prepend(__('Medicalshop ShopByBrand'));
        $resultPage->addBreadcrumb(__('Medicalshop'), __('Medicalshop'));
        $resultPage->addBreadcrumb(__('Items'), __('ShopByBrand'));
        return $resultPage;
    }
}
